Allows logic to be executed when entering/exiting media query aliases as set in CSS. Media queries can be maintained in a single location.
