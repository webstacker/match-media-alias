describe('matchMediaAlias', function() {

    it('should return a MediaAliasList', function() {

        var mal = matchMediaAlias('small');

        expect(mal instanceof MediaAliasList).toEqual(true);
    });

    it('should trigger when alias matches', function() {
        var triggered = false,
            mal = matchMediaAlias('small');

        mal.addListener(function(mal) {
            triggered = true;
        });

        matchMediaAlias._trigger('small');

        expect(triggered).toEqual(true);
    });

    it('should not trigger when alias is currently matched', function() {
        var triggeredCount = 0,
            mal = matchMediaAlias('small');

        mal.addListener(function(mal) {
            ++triggeredCount;
        });

        matchMediaAlias._trigger('small');
        matchMediaAlias._trigger('small');

        expect(triggeredCount).toEqual(1);
    });

    it('should exit currently matched breakpoints before entering newly matched breakpoints', function() {
        var expectedOrder = 'enter medium, exit medium, enter small, exit small, enter large',
            actualOrder = [],
            malA = matchMediaAlias('small'),
            malB = matchMediaAlias('medium'),
            malC = matchMediaAlias('large');

        malA.addListener(function(mal) {

            if (mal.matches) {
                actualOrder.push('enter small');
            } else {
                actualOrder.push('exit small');
            }
        });

        malB.addListener(function(mal) {

            if (mal.matches) {
                actualOrder.push('enter medium');
            } else {
                actualOrder.push('exit medium');
            }
        });

        malC.addListener(function(mal) {

            if (mal.matches) {
                actualOrder.push('enter large');
            } else {
                actualOrder.push('exit large');
            }
        });

        matchMediaAlias._trigger('medium');
        matchMediaAlias._trigger('small');
        matchMediaAlias._trigger('large');

        expect(actualOrder.join(', ')).toEqual(expectedOrder);
    });

    it('should exit currently matched breakpoints only if they don`t match the new breakpoint', function() {
        var expectedOrder = 'enter small medium, enter small, exit small medium, exit small, enter large',
            actualOrder = [],
            malA = matchMediaAlias('small'),
            malB = matchMediaAlias('small medium'),
            malC = matchMediaAlias('large');

        malA.addListener(function(mal) {

            if (mal.matches) {
                actualOrder.push('enter small');
            } else {
                actualOrder.push('exit small');
            }
        });

        malB.addListener(function(mal) {

            if (mal.matches) {
                actualOrder.push('enter small medium');
            } else {
                actualOrder.push('exit small medium');
            }
        });

        malC.addListener(function(mal) {

            if (mal.matches) {
                actualOrder.push('enter large');
            } else {
                actualOrder.push('exit large');
            }
        });

        matchMediaAlias._trigger('medium');
        matchMediaAlias._trigger('small');
        matchMediaAlias._trigger('large');

        expect(actualOrder.join(', ')).toEqual(expectedOrder);
    });
});

