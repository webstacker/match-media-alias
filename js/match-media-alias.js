(function(root, factory) {
    if (typeof define === 'function' && define.amd) {
        // AMD. Register as an anonymous module.
        define(['media-alias', 'media-alias-list'], factory);
    } else if (typeof module === 'object' && module.exports) {
        // Node. Does not work with strict CommonJS, but
        // only CommonJS-like environments that support module.exports,
        // like Node.
        module.exports = factory(require('media-alias'), require('media-alias-list'));
    } else {

        // Browser globals (root is window)
        root.matchMediaAlias = factory(root.mediaAlias, root.MediaAliasList);
    }
}(this, function(mediaAlias, MediaAliasList) {
    'use strict';

    var malCollection = [],
        currentlyMatchedOrder = [],
        matchMediaAliasListener = function() {
            matchMediaAlias._trigger(mediaAlias());
        };

    function matchMediaAlias(alias) {

        var mal = new MediaAliasList(alias);

        malCollection.push(mal);

        return mal;
    }
    matchMediaAlias._trigger = function(alias) {

        var mal,
            i;

        //exit currently matched aliases first
        for (i = 0; i < currentlyMatchedOrder.length; i++) {
            mal = currentlyMatchedOrder[i];

            //this mqEvent doesn't match the new mqEvent
            if (mal._aliases.indexOf(alias) === -1) {
                currentlyMatchedOrder.splice(i, 1);
                mal.matches = false;
                mal._trigger();
                i--;
            }
        }

        //enter newly matched aliases second
        for (i = 0; i < malCollection.length; i++) {
            mal = malCollection[i];

            //this mqEvent is not currently matched && this mqEvent matches the new mqEvent
            if (!mal.matches && mal._aliases.indexOf(alias) >= 0) {
                currentlyMatchedOrder.push(mal);
                mal.matches = true;
                mal._trigger();
            }
        }
    };


    if (window.addEventListener) {
        window.addEventListener('click', matchMediaAliasListener, false);
    } else if (window.attachEvent)  {
        window.attachEvent('onclick', matchMediaAliasListener);
    }

    return matchMediaAlias;
}));









